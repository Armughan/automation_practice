# To run the tests:

1. Clone the repository.
2. Install/Update Google Chrome to the latest version.
3. Run `npm install` in the cloned repository.
4. Run `node automation-test.js`.

