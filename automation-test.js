require('chromedriver');
const {Builder, By, Key, until} = require('selenium-webdriver');
const {Options} = require('selenium-webdriver/chrome');

let options = new Options();
options.addArguments(
    // 'headless',
    // '--user-data-dir=/home/ibnyusrat/.config/google-chrome/Default'
    // Use --disable-gpu to avoid an error from a missing Mesa library, as per
    // https://chromium.googlesource.com/chromium/src/+/lkgr/headless/README.md
    // disable-gpu',
);


(function beginCrawling() {
    openAutomationPractice().then();
})();

async function openAutomationPractice() {
    let driver = await new Builder().forBrowser('chrome').setChromeOptions(options).build();
    try {
        await driver.get('http://automationpractice.com/index.php');  /*open the site*/
        await driver.wait(until.titleIs('My Store'));     /*search title My store */
        await driver.findElement(By.name('search_query')).sendKeys("black", Key.ENTER);
        await driver.wait(until.titleIs('Search - My Store'));
        await driver.sleep(2000);
        await driver.findElement(By.xpath("//*[@id=\"center_column\"]//ul//li[1]//div//div[1]//div//a[1]//img")).click(); /*click on the item */
        await driver.sleep(2000);
        await driver.findElement(By.xpath("//*[@id=\"quantity_wanted_p\"]//a[2]")).click(); /*press incrementer*/
        await driver.sleep(2000);
        await driver.findElement(By.id("group_1")).click().then(async value => {                       /*select medium size*/
            await driver.findElement(By.xpath("//*[@id=\"group_1\"]//option[2]")).click()
        });
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"color_11\"]")).click();       /*select color*/
        await driver.sleep(1000);
        await driver.findElement(By.id("add_to_cart")).click();                       /* add to cart*/
        await driver.sleep(1000);
        await driver.findElement(By.id("layer_cart"));                                 /* checking pop up  is displayed or not*/
        await driver.sleep(1000);
        await driver.findElement(By.xpath("//*[@id=\"layer_cart\"]//div[1]//div[2]//div[4]//a")).click();
        await driver.sleep(1000);
        await driver.findElement(By.xpath("//*[@id=\"product_5_26_0_0\"]/td[1]/a/img"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"product_5_26_0_0\"]//td[2]"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"product_5_26_0_0\"]//td[3]"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"product_5_26_0_0\"]//td[4]"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"product_5_26_0_0\"]//td[5]"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"product_5_26_0_0\"]//td[6]"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"center_column\"]//p[2]//a[1]")).click();         /* proceed to checkout for signin*/
        await driver.sleep(1000);
        /*Sign in a User*/
        await driver.findElement(By.id("email")).sendKeys('ibnymunsib@gmail.com');
        await driver.sleep(500);
        await driver.findElement(By.id("passwd")).sendKeys('letmein');
        await driver.sleep(500);
        await driver.findElement(By.id("SubmitLogin")).click();                                         /*proceed to checkout for address checking or varifying*/
        await driver.sleep(1000);
        await driver.findElement(By.xpath("//*[@id=\"address_delivery\"]//li[4]"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"address_invoice\"]//li[4]"));
        await driver.sleep(500);
        await driver.findElement(By.name("processAddress")).click();                                    /*process the address and checkout for shipping */
        await driver.sleep(500);
        await driver.findElement(By.name("cgv")).click();
        await driver.sleep(500);
        await driver.findElement(By.name("processCarrier")).click();
        await driver.sleep(500);
        await driver.findElement(By.className("bankwire")).click();
        await driver.sleep(500);
        await driver.findElement(By.id("amount"));
        await driver.sleep(500);
        await driver.findElement(By.name("currency_payement"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"center_column\"]//form//div//p[4]"));
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"cart_navigation\"]//button")).click();
        await driver.sleep(500);
        await driver.findElement(By.xpath("//*[@id=\"center_column\"]/div/p"));     /* verifying Order on My Store is completed */
    } catch (err) {
        console.log('An Error Occured:');
        console.log('\x1b[41m%s\x1b[0m', err);
    } finally {
        console.log('Quitting.');
        driver.quit().then();
    }
}


